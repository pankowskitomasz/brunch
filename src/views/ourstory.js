import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import OurstoryS1 from "../components/ourstory-s1";
import OurstoryS2 from "../components/ourstory-s2";
import OurstoryS3 from "../components/ourstory-s3";

class Ourstory extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <OurstoryS1/>
                <OurstoryS2/>
                <OurstoryS3/>
            </Container>    
        );
    }
}

export default Ourstory;